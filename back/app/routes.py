from app import app
from app.reco.recommandations import Recommandations
from flask import Flask, render_template, jsonify, request
from surprise import dump



@app.route('/')
def accueil():
    return 'Accueil'


predictions, algo = dump.load(r"C:\Users\utilisateur\Desktop\my_tonight_movie\back\app\reco\dump_file")
reco = Recommandations()
top_n = reco.get_top_n(predictions, 10)


#Récupérer les n meilleures recommandations pour un utilisateur donné (userId)
@app.route('/recommandations/<int:userId>', methods=['GET'])
def index(userId):
    return jsonify(reco.top_n_u(top_n,userId))


@app.errorhandler(404)
def not_found(error):
    return jsonify({"msg": "RESOURCE NOT FOUND"})
