#Effectuer les recommandations à partir du dump file généré par la SVD sur l'utility matrix

from surprise import dump
from collections import defaultdict
import pickle

class Recommandations:

    def __init__(self):
        pass
    
    predictions, algo = dump.load(r"C:\Users\utilisateur\Desktop\my_tonight_movie\back\app\reco\dump_file")
    def get_top_n(self, predictions, n=10):
        
        '''Retourne les n meilleures prédictions pour les utilisateurs.

        Arguments :
        
        predictions(Un objet liste de prédiction): La liste de prédiction retournée par la méthode test d'un algorithme.
        n(int): Le nombre de recommandations à donner en sortie pour chaque utilisateur. 10 par défault

        Retourne:
        
        Un dictionnaire ou les clés sont les utilisateurs (raw) id et les valeurs sont des listes de tuples :
        [(raw item id, rating estimation), ...] de taille n.
        '''
        # Assigner les prédictions pour chaque utilisateur.
        top_n = defaultdict(list)
        for uid, iid, true_r, est, _ in predictions:
            top_n[uid].append((iid, est))

        # Puis on trie les prédictions pour chaque utilisateur et on récupère les k plus grandes.
        for uid, user_ratings in top_n.items():
            user_ratings.sort(key=lambda x: x[1], reverse=True)
            top_n[uid] = user_ratings[:n]

        return top_n
    
    def top_n_u(self, top_n, u):
        ''' Retourne les meilleures prédictions pour un utilisateur u(userId) sous la forme d'un tableau de dictionnaires'''
        pred = []
        for i in top_n[u]:
            tmp = {'movieId' : i[0], 'est': i[1]}
            pred.append(tmp)
        return pred

