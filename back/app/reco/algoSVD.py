import pandas as pd

from surprise import Dataset, Reader, SVD, dump
from surprise.model_selection import cross_validate
from surprise.model_selection import GridSearchCV

#Charger le dataset avec Pandas
ratings = pd.read_csv(r"C:\Users\utilisateur\Desktop\my_tonight_movie\back\app\reco\data\ratings_small.csv")

#Définir l'échelle de notation.
reader = Reader(rating_scale=(1, 5))

#Les colonnes doivent correspondre à l'id utilisateur, l'id du film et la note (dans cet ordre)
data = Dataset.load_from_df(ratings[['userId', 'movieId', 'rating']], reader)

#Optimisation des paramètres (On recherche les meilleurs paramètres à partir d'un ensemble de paramètres sélectionnés)
param_grid = {'n_epochs': [5, 10], 'lr_all': [0.002, 0.005],
              'reg_all': [0.4, 0.6]}
gs = GridSearchCV(SVD, param_grid, measures=['rmse', 'mae'], cv=3)

gs.fit(data)

#Sélectionner le meilleur jeu de paramètres :
algo = gs.best_estimator['rmse']
trainset = data.build_full_trainset()
algo.fit(trainset)


# Puis on effectue les prédictions pour toutes les paires utilisateurs/films qui ne sont pas dans le training set.
testset = trainset.build_anti_testset()
predictions = algo.test(testset)


dump.dump(r"C:\Users\utilisateur\Desktop\my_tonight_movie\back\app\reco\dump_file", predictions, algo)
