﻿Environnement virtuel avec anaconda:

1. Importer l'environnement à partir du fichier

>>> conda env create -f environment.yml

2. Exporter les specs de l’environnement :

>>> conda env export > environment.yml


Lancer l’environnement : 

conda activate movie_project
