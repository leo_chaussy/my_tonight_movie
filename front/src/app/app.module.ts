import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NowPlayingListComponent } from './components/now-playing-list/now-playing-list.component';
import { HttpClientModule } from '@angular/common/http';
import { HelloWorldComponent } from './components/hello-world/hello-world.component';
import { TrucComponent } from './components/truc/truc.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {NgxYoutubePlayerModule} from 'ngx-youtube-player';
import {SimilarComponentComponent } from './components/similar-component/similar-component.component';
import { CalibrageComponent } from './components/calibrage/calibrage.component';




@NgModule({
  declarations: [
    AppComponent,
    NowPlayingListComponent,
    HelloWorldComponent,
    TrucComponent,
    NotFoundComponent,
     SimilarComponentComponent,
     CalibrageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatToolbarModule,
    MatCardModule,
    NgxYoutubePlayerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
