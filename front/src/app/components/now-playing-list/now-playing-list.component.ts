import { Component, OnInit } from '@angular/core';
import { MovieDBService } from 'src/app/services/movie-db.service';
import { getAttrsForDirectiveMatching } from '@angular/compiler/src/render3/view/util';
import { Title } from '@angular/platform-browser';
import { MyMovieService } from 'src/app/services/my-movie.service';

@Component({
  selector: 'app-now-playing-list',
  templateUrl: './now-playing-list.component.html',
  styleUrls: ['./now-playing-list.component.css']
})
export class NowPlayingListComponent implements OnInit {

  movies:any;

  constructor(public movieDbService : MovieDBService, public myMovieService : MyMovieService) { }

  ngOnInit() {
    
    this.movieDbService.getNowPlayingMovies().subscribe((res:any)=>{
      console.log(res);
      this.movies=res.results;

    }, (err)=>{
      console.log(err);



    })   

  }

  myFavorite (movieId,title, overview) {
    const Film = {
      type: movieId,
      title: title,
      description : overview,
      duree : 90
    }
this.myMovieService.addToFavorite(Film).subscribe((res:any)=>{
  console.log(res);
}, (err:any) => {
  console.log(err);
})  }
  
}
