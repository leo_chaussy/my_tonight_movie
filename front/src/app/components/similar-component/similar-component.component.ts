import { Component, OnInit } from '@angular/core';
import { MovieDBService } from 'src/app/services/movie-db.service';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-similar-component',
  templateUrl: './similar-component.component.html',
  styleUrls: ['./similar-component.component.css']
})
export class SimilarComponentComponent implements OnInit {

  constructor(public movieDbService : MovieDBService, private route : ActivatedRoute) { }
    
   
  movies:any;
  movieId

  ngOnInit() {
    
    this.movieId  = this.route.snapshot.paramMap.get("id")

    console.log(this.movieId)
    
    this.movieDbService.getSimilarMovie(+this.movieId).subscribe((res:any)=>{
      console.log(res);
      this.movies=res.results;
      
    }, (err)=>{
      console.log(err);
    })
    this.route = null
  }



}



