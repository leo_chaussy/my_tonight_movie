import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimilarComponentComponent } from './similar-component.component';

describe('SimilarComponentComponent', () => {
  let component: SimilarComponentComponent;
  let fixture: ComponentFixture<SimilarComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimilarComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimilarComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
